package jp.android_group.student.abc2015summer;

import android.view.View;

public class Configuration {
    //================================
    // マイスケジュール登録ボタンの表示非表示フラグ
    // View.VISIBLE or View.GONE
    //現在バザールのみ手打ちでGONE
    //================================
    public static final int favoriteButtonIsVisible = View.VISIBLE,
            CALENDAR_YEAR = 2015,
            CALENDAR_MONTH = 7,
            CALENDAR_DATE = 20,
            CALENDAR_MINUTE = 0;
}
